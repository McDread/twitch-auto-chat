#!/bin/python

import requests
import secrets

def clip_get():
    print("Clipping last 30 secs...", end=" ")
    clip_response = requests.post(secrets.CLIP_URL, headers=secrets.UPDATE_HEADERS)
    if str(clip_response) == "<Response [202]>":
        clip_id = str(clip_response.json()['data'][0]['id'])
        with open("".join(secrets.WORK_DIR, ["clip_edit.txt"]), "a") as f:
            f.write("".join(["\n", "https://clips.twitch.tv/", clip_id, "/edit"]))
        print("created clip: ", clip_id)
        return "".join(["https://clips.twitch.tv/", clip_id])
    else:
        print("no clips! because of: ", clip_response, clip_response.content)
        return("https://twitch.tv/cidermcdread/clips")


