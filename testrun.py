#!/bin/python

import socket
import secrets
import time

print("Connecting to twitch host...", end=" ")
s = socket.socket()
s.connect((secrets.HOST, secrets.PORT))
s.send(bytes("PASS " + secrets.PASS_t + "\r\n", "UTF-8"))
s.send(bytes("NICK " + secrets.NICK_t + "\r\n", "UTF-8"))
s.send(bytes("JOIN #" + secrets.CHANNEL + " \r\n", "UTF-8"))
s.send(bytes("CAP REQ :twitch.tv/tags\r\n", "UTF-8"))
s.send(bytes("CAP REQ :twitch.tv/membership\r\n", "UTF-8"))
print("Connected to chat:", secrets.CHANNEL, "\n")

def send_message(msg):
    s.send(bytes("PRIVMSG #" + secrets.CHANNEL + " :" + msg + "\r\n", "UTF-8"))

def test_run():
    print("Initializing testrun...", end="  ")
    send_message("----Hold on to your hats, we're going on a test run!----")
    time.sleep(1)
    send_message("!quote")
    time.sleep(1)
    send_message("!clip")
    time.sleep(1)
    send_message("!define define")
    time.sleep(3)
    send_message("!freegame")
    time.sleep(4)
    send_message("!joke")
    time.sleep(3)
    send_message("!inspiration")
    time.sleep(4)
    send_message("!mcdead")
    time.sleep(2)
    send_message("!title Playing Neglected Games")
    time.sleep(1)
    send_message("!skipsong")
    time.sleep(1)
    send_message("!todo Go to sleep")
    time.sleep(2)
    send_message("!addquote Please !delquote this")
    time.sleep(1)
    send_message("Test run finished, manually run !delquote [num] ")
    print("Finished testrun")

test_run()
