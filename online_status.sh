curl -s -H "Authorization: Bearer $twitch_auth" -H "Client-ID: $twitch_client_id" -X GET "https://api.twitch.tv/helix/streams?user_login=$twitch_user" | jq ".data[].type" | cut -d '"' -f 2
