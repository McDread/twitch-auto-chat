#!/bin/python

import re

#URL for generating Token(check adress line in browser response, redirect your Client to http://localhost from twitch dev)
#https://id.twitch.tv/oauth2/authorize?client_id=<CLIENT ID>&redirect_uri=http://localhost&response_type=token&scope=channel:manage:broadcast%20clips:edit
#https://dev.twitch.tv/docs/authentication#scopes <- for scopes


WORK_DIR = '<path to working dir>'
GET_GAME_SCRIPT = '<path to get_open_game_window.sh>'
SKIP_SONG_SCRIPT = '<path to skipsong.sh>'

DB_FILE_PATH = "".join([WORK_DIR, "db_twitchbot.db"])
HOST = 'irc.chat.twitch.tv'
PORT = 6667
CHANNEL = '<channel username>'
NICK = '<chatbots username>' #case sensitive
PASS = 'oauth:<irc oauth for reading/posting in chat for NICK username>'
NICK_t = '<streamers username>' 
PASS_t = '<streamers oauth>' #only needed if you want to chat as "yourself" instead of bot


#The following are needed for updating channel Title and Game Category
UPDATE_URL = 'https://api.twitch.tv/helix/channels?broadcaster_id=<channel id>'
CLIP_URL = 'https://api.twitch.tv/helix/clips?broadcaster_id=<channel id>'
UPDATE_HEADERS = {'Authorization': 'Bearer <Token for Client>',
                  'Client-Id': '<Client ID>'}

#A list of regular expressions that the bot will not interact with
BLOCKLIST = [
re.compile(r"bigfollows*", re.IGNORECASE),
re.compile(r"twitchfollowers*", re.IGNORECASE),
re.compile(r"Wanna become famous*", re.IGNORECASE),
re.compile(r"(\*com)", re.IGNORECASE),
re.compile(r"(\*\s*c\s*o\s*m)", re.IGNORECASE),
re.compile(r"f[^string]*?a[^string]*?g[^string]*?", re.IGNORECASE),
re.compile(r"f[^string]*?a[^string]*?g[^string]*?g[^string]*?o[^string]*?t", re.IGNORECASE),
re.compile(r"n[^string]*?i[^string]*?g[^string]*?g[^string]*?e[^string]*?r", re.IGNORECASE),
re.compile(r"n[^string]*?e[^string]*?g[^string]*?r[^string]*?o", re.IGNORECASE),
re.compile(r"d[^string]*?y[^string]*?k[^string]*?e", re.IGNORECASE),
re.compile(r"c[^string]*?h[^string]*?i[^string]*?n[^string]*?k", re.IGNORECASE),
re.compile(r"c[^string]*?o[^string]*?o[^string]*?n", re.IGNORECASE),
re.compile(r"t[^string]*?r[^string]*?a[^string]*?n[^string]*?n[^string]*?i[^string]*?e", re.IGNORECASE),
re.compile(r"t[^string]*?r[^string]*?a[^string]*?n[^string]*?n[^string]*?y", re.IGNORECASE),
re.compile(r"r[^string]*?e[^string]*?t[^string]*?a[^string]*?r[^string]*?d", re.IGNORECASE),
re.compile(r"s[^string]*?a[^string]*?n[^string]*?d[^string]*?m[^string]*?o[^string]*?n[^string]*?k[^string]*?e[^string]*?y", re.IGNORECASE)
]



