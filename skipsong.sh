#!/bin/sh

WINDOW=$(xwininfo -root -tree | grep "Pretzel")
X=$(echo $WINDOW | cut -d "+" -f 4)
Y=$(echo $WINDOW | cut -d "+" -f 5)

X=$(awk -v X=$X 'BEGIN{print X+150}')
Y=$(awk -v Y=$Y 'BEGIN{print Y+250}')

xdotool mousemove $X $Y key Right mousemove restore
