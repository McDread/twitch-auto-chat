## A very Simple Twitch chatbot and channel updater for Linux

### Installation
Create the **_secrets.py_** file based on the **_fakesecrets.py_** file.  
Change the "if message.startswith" commands in the bottom of **_chatbot.py_** for different !commands.  
If you want to autoupdate game category on your stream you probably need to keep an eye on the **_get\_open\_game\_windows.sh_** based on your needs.  

Do note that this bot is made by me for me and will probably need some work if someone actually would want to use it #WorksOnMyMachine

#### Dependencies
 - Sqlite3 module for python
 - feedparser

For the autoupdate:
 - dmenu
 - xwininfo
 - mpv (for playing audio/video clips)

Optional:
 - xdotool (!skipsong functionality)
 - Pretzel Rocks (!skipsong functionality)

### Usage
I recommend doing it from terminal to keep an eye on outputs:
 - python chatbot.py
 - python update\_game\_category.py


#### FAQ
[Come ask at twitch](https://www.twitch.tv/cidermcdread)
