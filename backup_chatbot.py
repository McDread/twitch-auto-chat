#!/bin/python

import feedparser
import os
import random
import re
import requests
import socket
import subprocess
import sqlite3
import secrets #check "fakesecrets.py" for format
import time


if os.path.isfile(secrets.DB_FILE_PATH):
    db = sqlite3.connect(secrets.DB_FILE_PATH)
    cur = db.cursor()
    print("connected to local db:", secrets.DB_FILE_PATH)
else:
    db = sqlite3.connect(secrets.DB_FILE_PATH)
    cur = db.cursor()
    cur.execute("CREATE TABLE death_counter (game_name TEXT, game_id INTEGER, count INTEGER)")
    cur.execute("CREATE TABLE game_ids (query TEXT, game_id INTEGER, game_name TEXT)")
    cur.execute("CREATE TABLE quote_strings (id INTEGER PRIMARY KEY, quote TEXT NOT NULL)")
    cur.execute("INSERT INTO quote_strings (quote) VALUES (?)", ["Don't Panic"]) 
    db.commit()
    print("Created local db:", secrets.DB_FILE_PATH)


print("Connecting to twitch host...", end=" ")
s = socket.socket()
s.connect((secrets.HOST, secrets.PORT))
s.send(bytes("PASS " + secrets.PASS + "\r\n", "UTF-8"))
s.send(bytes("NICK " + secrets.NICK + "\r\n", "UTF-8"))
s.send(bytes("JOIN #" + secrets.CHANNEL + " \r\n", "UTF-8"))
s.send(bytes("CAP REQ :twitch.tv/tags\r\n", "UTF-8"))
s.send(bytes("CAP REQ :twitch.tv/membership\r\n", "UTF-8"))
print("Connected to chat:", secrets.CHANNEL, "\n")

def clip_get():
    print("Clipping last 30 secs...", end=" ")
    clip_response = requests.post(secrets.CLIP_URL, headers=secrets.UPDATE_HEADERS)
    if str(clip_response) == "<Response [202]>":
        clip_id = str(clip_response.json()['data'][0]['id'])
        with open("".join([secrets.WORK_DIR, "clip_edit.txt"]), "a") as f:
            f.write("".join(["https://clips.twitch.tv/", clip_id, "/edit", "\n"]))
        print("created clip: ", clip_id)
        return "".join(["https://clips.twitch.tv/", clip_id])
    else:
        print("no clips! because of: ", clip_response, clip_response.content)
        return("https://twitch.tv/cidermcdread/clips")

def death_counter_get(current_game_id, current_game_name):
    print("death counter...", end=" ")
    cur.execute("SELECT * FROM death_counter WHERE game_id=?", [current_game_id])
    sqlite_result = cur.fetchone()
    if sqlite_result == None:
        cur.execute("INSERT INTO death_counter VALUES (?, ?, ?)", (current_game_name, current_game_id, 0))
        db.commit()
        print("new death db entry: ", current_game_name, "\n")
        return 0
    else:
        deaths = sqlite_result[2]
        print("current deaths: ", deaths, current_game_name, "\n")
        return deaths

def free_games(get=False):
    print("Checking for free games... ", end=" ")
    feed = feedparser.parse("https://old.reddit.com/r/FreeGameFindings/new/.rss").entries
    r = requests.get("https://www.gamerpower.com/api/giveaways?&type=game").json()
    cur.execute("SELECT * FROM free_games")
    db_data = cur.fetchall()
    free_games_list = [i[1] for i in db_data]
    for i in range(5, -1, -1):
        try:
            game_fgf = feed[i].title
            game_gp =  " - ".join([r[i]['title'].strip("Free "), r[i]['platforms'].strip("DRM-Free")])
        except:
            game_fgf = "Not found"
            game_gp = "Not found"
        if game_fgf not in free_games_list:
            source = "https://old.reddit.com/r/FreeGameFindings/new/"
            cur.execute("INSERT INTO free_games(game_info, source) VALUES (?, ?)", (game_fgf, source))
            send_message("".join([game_fgf, 'is reported as free by ', source]))
        if game_gp not in free_games_list:
            source = "https://www.gamerpower.com/" 
            cur.execute("INSERT INTO free_games(game_info, source) VALUES (?, ?)", (game_gp, source))
            send_message(" ".join([game_gp, 'is reported as free by', source]))
    if get == True:
        time.sleep(1) 
        send_message(" ".join(["Last reported free games:", db_data[-1][1], "by", db_data[-1][2], "      and:", db_data[-2][1], "by", db_data[-2][2]]))
    db.commit()
    print("Games reported.")

def inspiroquote_get():
    print("getting inspiroquote... ", end=" ")
    INSPIROBOT = requests.get("https://inspirobot.me/api?generateFlow=1&sessionID=" + requests.get("https://inspirobot.me/api?getSessionID=1").text)
    INSPIROQUOTE = INSPIROBOT.json()["data"][1]["text"]
    if len(INSPIROQUOTE) > 180:
        INSPIROQUOTE = INSPIROBOT.json()["data"][3]["text"]
    quote = "".join(['"', INSPIROQUOTE.replace("\n", " ").replace("[", " [").strip(), '"'])
    send_message(quote)
    return quote

def joke_get():
    with open("".join([secrets.WORK_DIR,'jokes.txt']), 'r') as f:
        joke = random.choice([i for i in f.readlines()])
    return joke

def update_title(message):
    print("Updating title...", end="  ")
    new_title = message.split('!title')[1][1:]
    title = "".join([new_title, " <deaths=", str(death_counter_get(current_game_id, current_game_name)), "> |Philosophy|Psychology|Gaming|GNU/Linux|"])
    updated_title_response = requests.patch(secrets.UPDATE_URL, data={'title': title}, headers=secrets.UPDATE_HEADERS)
    print("Updated title: ", title, updated_title_response)
    return message

def quote_add(message, cat="quote_strings"):
    print(cat, "Adding Quote...", end="  ")
    if len(message) < 11:
        send_message("Sorry, that quote was a bit too short")
        return
    else:
        quote = message.split('!addquote')[1][1:] 
        cur.execute("INSERT INTO quote_strings (quote) VALUES (?)", [quote])
    db.commit()
    cur.execute("SELECT * FROM sqlite_sequence")
    id_max = cur.fetchall()
    send_message(" ".join(["Thanks for your contribution! [", str(id_max[0][1]), "]"]))
    print("Added Quote: ", quote)

def quote_del(message, quote_id):
    print("Deleting quote...", end=" ")
    quote_num = message.split()[-1]
    if quote_num == "!delquote":
        cur.execute("SELECT * FROM quote_strings WHERE id=?", [quote_id])
    else:
        cur.execute("SELECT * FROM quote_strings WHERE id=?", [quote_num])
    quote_data = cur.fetchone()
    try:
        cur.execute("DELETE FROM quote_strings WHERE id=?", [quote_data[0]])
        send_message("".join(["Deleted quote: ", str(quote_data)]))
        print("deleted quote: ", quote_data)
        cur.execute("SELECT * FROM sqlite_sequence")
        id_max = cur.fetchall()[0][1]
        cur.execute("UPDATE sqlite_sequence SET seq=? WHERE name='quote_strings'", [id_max])
        db.commit()
    except:
        send_message("Did not find that quote, sorry.")

def quote_get(message, nick_sender):
    print("Selecting Quote...", end="  ")
    cur.execute("SELECT * from sqlite_sequence")
    id_max = cur.fetchall()
    quote_num = message.split()[-1]
    if quote_num == "!quote":
        cur.execute("SELECT * FROM quote_strings ORDER BY RANDOM()")
    else:
        cur.execute("SELECT * FROM quote_strings WHERE id=?", [quote_num])
    try:
        li_quote = cur.fetchone()
        outgoing = "".join(['"', li_quote[1], '" [', str(li_quote[0]),'/', str(id_max[0][1]), ']'])
    except:
        outgoing = "Could not find a quote, sorry!"
    if "%nick" in outgoing:
        outgoing = outgoing.replace("%nick", nick_sender)
    send_message(outgoing)
    print("Quote '", outgoing ,"' sent \n")
    try:
        return li_quote[0]
    except:
        return 0

def send_message(msg):
    if len(msg) > 499:
        s.send(bytes("PRIVMSG #" + secrets.CHANNEL + " :" + "Sorry, my payload was to long and hard to insert into the receptacle..." + "\r\n", "UTF-8"))
    else:
        s.send(bytes("PRIVMSG #" + secrets.CHANNEL + " :" + msg + "\r\n", "UTF-8"))

def stream_info_get():
    r = requests.get(secrets.UPDATE_URL, headers=secrets.UPDATE_HEADERS).json()
    game_id = r['data'][0]['game_id']
    game_name = r['data'][0]['game_name']
    stream_title = r['data'][0]['title']
    return game_id, game_name, stream_title

def todo_add(message):
    with open("".join([secrets.WORK_DIR, "TODO.txt"]), "a") as f:
        f.write("".join(["\n", message]))
    send_message("Thank you for your request!")

def word_define(message):
    word = message.split("!define ")[1].split(" ")
    try:
        num_def = int(word[1])
    except:
        num_def = 1
    definition_list = [i for i in word_definitions_list(word[0]) if i != []]
    if num_def > len(definition_list):
        msg_to_send = " ".join(["Sorry, the number of definitions are:", str(len(definition_list))])
    elif 1 > len(definition_list):
        msg_to_send = "Sorry, no definitions found"
    else:
        msg_to_send = " ".join([" - ".join(definition_list[num_def-1]), "[", str(num_def), "/", str(len(definition_list)), "]" ])
    send_message(msg_to_send)

def word_definitions_list(word, d="-wn"):
    print("defining: ", end=" ")
    subp = subprocess.Popen(["dict", "-d", d, word], bufsize=1, universal_newlines=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    definition = subp.communicate()[0]
    time.sleep(0.5)
    defs_list = []
    x = 1
    while x < 10:
        try:
            defs_list.append(["".join([re.sub(" +", " ",i.replace("\n", " ")),  " - "]) for i in re.findall("\d:\D+", definition) if i.startswith(str(x))])
            x += 1
        except:
            break
    return defs_list


current_game_id, current_game_name, current_stream_title  = stream_info_get()
print(current_game_id, current_game_name, current_stream_title)
current_title = "!title " + current_stream_title.split("<deaths")[0]
message = ""
nick_sender = ""
word = ""
death_counter = 0
ts = 0
ts_title = 0
ts_dead = 0
ts_def = 0
ts_quote = 0
ts_delquote = 0
ts_addquote = 0
ts_clip = 0
ts_joke = 0
ts_free = 0
ts_inspiration = 0
ts_song = 0
ts_todo = 0
ts_command = 0
ts_vid = 0
quote_id = "!delquote"
is_mod = False

while True:
    for line in str(s.recv(1024)).split('\\r\\n'):
        if "PING :tmi.twitch.tv" in line:
            s.send(bytes("PONG :tmi.twitch.tv\r\n", "UTF-8"))

        re_ts = re.search('ts=\d+', line)
        if re_ts:
            ts = int(re_ts.group().split("=")[1])
        re_mod = re.search('badges=moderator', line) #use 'if re_mod:' on commands exclusive for mods
        re_nick = re.search('display-name=\w+;', line)
        if re_nick:
            nick_sender = re_nick.group().split("=")[1][:-1]

        re_mess = re.search(' #\w+ .+', line)
        if re_mess:
            li_mess = re_mess.group().split(":", 1)
            message = li_mess[-1]

        for i in secrets.BLOCKLIST: 
            if re.match(i, message) != None:
                print("Blocklist caught: ", message, "\n")
            else:
                ##### GIVE COMMANDS TO THE BOT #####

                if message.startswith("!addquote") and "/" not in message and ts-ts_addquote > 10000: #10 seconds
                    ts_quote = ts
                    quote_add(message, cat="quotes")

                if message.startswith("!bot") and ts-ts_quote > 10000: #10 seconds
                    ts_quote = ts
                    send_message("This bot exists at: https://gitlab.com/McDread/twitch-auto-chat #WorksOnMyMachine")

                if message.startswith("!clip") and ts-ts_clip > 10000: #10 seconds
                    ts_clip = ts
                    clip_url = clip_get()
                    send_message(clip_url)

                if message.startswith("!command") and ts-ts_command > 10000: #10 seconds
                    ts_quote = ts
                    send_message('''Available commands: !addquote [%nick], !bot, !clip, !define [word][n], !freegame !inspiration !joke !mcdead, !quote [n] !title [new title], !todo [request], !skipsong''')

                if (message.startswith("!define") or message.startswith("!dict") ) and ts-ts_def > 5000: #5 seconds
                    ts_def = ts
                    word_define(message)
               
                if message.startswith("!delquote") and ts-ts_delquote > 10000 and (nick_sender == secrets.NICK_t or re_mod): #10 seconds
                    ts_delquote = ts
                    quote_del(message, quote_id) 

                if message.startswith("!freegame") and ts-ts_free > 10000: #10 seconds
                    ts_free = ts
                    free_games(get=True)
               
                if message.startswith("!inspiration") and ts-ts_quote > 10000: #10 seconds
                    ts_inspiration = ts
                    print("Inspiration: ", inspiroquote_get())

                if message.startswith("!joke") and ts-ts_joke > 100000: #100 seconds
                    ts_joke = ts
                    print(" ------ \n ------", joke_get(), "------")

                if (message.startswith("!mcdead") or message.startswith("!mcdeath") or message.startswith("!deathcount")) and ts-ts_dead > 10000: #10 seconds
                    ts_dead = ts
                    current_game_id, current_game_name, current_stream_title  = stream_info_get()
                    death_counter = death_counter_get(current_game_id, current_game_name)
                    send_message(" ".join([current_game_name, "current deaths: ", str(death_counter)]))

                if message.startswith("!quote") and ts-ts_quote > 10000: #10 seconds
                    ts_quote = ts
                    quote_id =quote_get(message, nick_sender)

                if message.startswith("!title") and ts-ts_title > 10000: #10 seconds
                    ts_title = ts
                    current_title = update_title(message)
                    send_message("Changed Title")

                if (message.startswith("!todo") or message.startswith("!request")) and ts-ts_todo > 10000: #10 seconds
                    ts_todo = ts
                    todo_add(message)

                if message.startswith("!skipsong") and ts-ts_command > 10000: #10 seconds WILL ONLY WORK WITH PRETZEL IF IT WORKS AT ALL
                    ts_song = ts
                    print("Skipping Pretzel song...", end=" ")
                    song_sub = subprocess.run(["/usr/bin/sh", "/home/mcdread/stuff/twitch-auto/skipsong.sh"])
                    print("Song skipped \n")

                if ts-ts_free > 600000: #600 seconds
                    ts_free = ts
                    free_games()

                message = ""
        

                 
