#!/bin/python

import sqlite3
import sys
import requests
import secrets

db = sqlite3.connect(secrets.DB_FILE_PATH)
cur = db.cursor()

def clip_get():
    clip_response = requests.post(secrets.CLIP_URL, headers=secrets.UPDATE_HEADERS)
    if str(clip_response) == "<Response [202]>":
        clip_id = str(clip_response.json()['data'][0]['id'])
        with open("".join(secrets.WORK_DIR, ["clip_edit.txt"]), "a") as f:
            f.write("".join(["https://clips.twitch.tv/", clip_id, "/edit", "\n"]))
        return "".join(["https://clips.twitch.tv/", clip_id])
    else:
        return("https://twitch.tv/cidermcdread/clips")

def death_set(value=1):
    cur.execute("SELECT * FROM death_counter WHERE game_id=?", [game_id])
    sqlite_result = cur.fetchone()
    if sqlite_result == None:
        cur.execute("INSERT INTO death_counter VALUES (?, ?, ?)", (game_name, game_id, value))
        deaths = value
    elif value != 1:
        cur.execute("UPDATE death_counter SET count=? WHERE game_id=?", (value, game_id))
        deaths = value
    else:
        cur.execute("UPDATE death_counter SET count=count+1 WHERE game_id=?", [game_id])
        deaths = sqlite_result[2]+1
    db.commit()
    return deaths

def stream_info_get():
    r = requests.get(secrets.UPDATE_URL, headers=secrets.UPDATE_HEADERS).json()
    game_id = r['data'][0]['game_id']
    game_name = r['data'][0]['game_name']
    stream_title = r['data'][0]['title']
    return game_id, game_name, stream_title

def update_title():
    title = "".join([new_title, " <deaths=", str(death_counter_update("getting")), "> |Philosophy|Psychology|Gaming|GNU/Linux|"])
    updated_title_response = requests.patch(secrets.UPDATE_URL, data={'title': title}, headers=secrets.UPDATE_HEADERS)


game_id, game_name, stream_title = stream_info_get()
if len(sys.argv) > 1:
   deaths = death_set(sys.argv[1])
else:
    deaths = death_set()

old_deaths = stream_title.split("=")[1].split(">")[0]
new_title = stream_title.replace(old_deaths, str(deaths))
updated_title_response = requests.patch(secrets.UPDATE_URL, data={'title': new_title}, headers=secrets.UPDATE_HEADERS)
clip_get()
