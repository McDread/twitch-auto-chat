#!/bin/python

import sqlite3
import requests
import subprocess
import os
import time
import random
import socket
import secrets # Check "fakesecrets.py" for format


deep_breath_files = [i for i in os.listdir("".join([secrets.WORK_DIR, "audio_video"])) if i.startswith("deep_breath")] #list of short videos that will be played at interval
current_query = "politics"
current_game_id = 515214
current_game_name = "Politics"

if os.path.isfile(secrets.DB_FILE_PATH):
    db = sqlite3.connect(secrets.DB_FILE_PATH)
    cur = db.cursor()
    print("connected to local db:", secrets.DB_FILE_PATH)
else:
    db = sqlite3.connect(secrets.DB_FILE_PATH)
    cur = db.cursor()
    cur.execute("CREATE TABLE death_counter (game_name TEXT, game_id INTEGER, count INTEGER)")
    cur.execute("CREATE TABLE death_strings (id INTEGER NOT NULL UNIQUE, quote TEXT, PRIMARY KEY(id AUTOINCREMENT))")
    cur.execute("CREATE TABLE game_ids (query TEXT, game_id INTEGER, game_name TEXT)")
    cur.execute("CREATE TABLE quote_strings (id INTEGER NOT NULL UNIQUE, quote TEXT NOT NULL, PRIMARY KEY(id AUTOINCREMENT))")
    cur.execute("INSERT INTO death_strings (quote) VALUES ('Well, that went well')")
    cur.execute("INSERT INTO quote_strings (quote) VALUES (?)", ["Don't Panic"]) 
    db.commit()
    print("Created local db:", secrets.DB_FILE_PATH)

def update_game_category(query):
    query = query.replace("Lutris", "")
    #query = "Nioh 2" for Manual updating game query
    if query == "": ####### change this to something random when making manual query
        query = "technology" ######  fallback category  #######
        print("Game category updated without a query term, therefore: ", query, "\n")
    cur.execute("SELECT * FROM game_ids WHERE query=?", [query])
    try:
        sqlite_result = cur.fetchone()
        game_id = sqlite_result[1]
        game_name = sqlite_result[2]
    except:
        #query = "Final Fantasy Xiv"  ###### For making a manual query
        print("No game id found in db, getting from API: ", query)
        r = requests.get('https://api.twitch.tv/helix/search/categories', params={'query': query}, headers=secrets.UPDATE_HEADERS).json()
        try:
            if len(r['data']) > 1:
                dmenu_str = "".join(["".join([i['name'], " ", i['id'], "\n"]) for i in r['data']])
                subp = subprocess.Popen(["dmenu", "-sb", "#f00", "-sf", "#000"], bufsize=1, universal_newlines=True, 
                        stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                try:
                    dmenu_result = subp.communicate(dmenu_str)[0]
                    game_id = dmenu_result.split()[-1]
                    game_name = " ".join(dmenu_result.split()[:-1])
                    print("game name is: ", game_name)
                except:
                    print("Exited dmenu without choice, doing nothing \n")
                    return current_game_id, current_game_name, query
            else:
                game_id = r['data'][0]['id']
                game_name = r['data'][0]['name']
        except:
            with open("".join([secrets.WORK_DIR, 'MANUAL_INTERVENTION.txt']), 'a') as f:
                f.write(query+"\n")
            print('API found nothing, manual db update needed: created file MANUAL_INTERVENTION.txt\n')
            return current_game_id, current_game_name, query
        cur.execute("INSERT INTO game_ids VALUES (?, ?, ?)", (query, game_id, game_name))
        db.commit()
        print("db insert: ", query, game_id, game_name)
    updated_game_response = requests.patch(secrets.UPDATE_URL, data={'game_id': game_id}, headers=secrets.UPDATE_HEADERS)
    print("Game category updated: ", game_id, game_name, updated_game_response.content, "\n")
    return game_id, game_name, query

ts_vid = time.time()-2100
while True:
    ts = time.time()
    subp = subprocess.Popen(["/usr/bin/sh", secrets.GET_GAME_SCRIPT],  bufsize=1, universal_newlines=True, stdout=subprocess.PIPE)
    query = subp.communicate()[0].split("\n")[0]
    print("Checking query: ", query, ts) ### If the query does not work you can manually add it to the local db
    if os.path.exists("/home/mcdread//politics_twitch.txt"): ## For always being in politics
        query = "politic"
        print("--- Politics toggle is ON! ---")
    if os.path.exists("/home/mcdread//technology_twitch.txt"): ## For always being in technology
        query = "technology"
        print("--- Technology toggle is ON! ---")
    if os.path.exists("/home/mcdread/chatting_twitch.txt"): ## For always being in technology
        query = "chatting"
        print("--- Just Chatting toggle is ON! ---")
    if query != current_query:
        current_game_id, current_game_name, current_query = update_game_category(query)
    #if ts-ts_vid > 2000: #2000
    #    ts_vid = ts
    #    subp = subprocess.run(["mpv",  "--geometry=480x360+1920+0/2", "--volume=70", "".join([secrets.WORK_DIR, "audio_video/", random.choice(deep_breath_files)])])
         
    time.sleep(60) 


