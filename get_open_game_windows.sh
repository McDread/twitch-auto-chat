#!/bin/sh

CURRENT_GAME=$(xwininfo -root -tree | grep -vE "xdg-desktop-portal|Clipboard|qutebrowser|Qutebrowser|Firefox|firefox|Steam|\"i3\":|i3bar|i3-frame|root window|no name|xterm|child|Parent|Chatterino|WebKitWebProcess|twitch|mcdread@arch|chatterino|obs|surf|lutris|epic|Default|clipboard|Alacritty|chiaki|rundll|uilding|DB Browser|secrets|mpv|zathura|Volume Control|discord|Discord|pavucontrol" | cut -d '"' -f 2 | sed '/^\s*$/d' | cut -d " " -f 1,2,3)


echo $CURRENT_GAME
